package ru.kata.spring.boot_security.demo.dao;

import ru.kata.spring.boot_security.demo.entity.MyUser;

import java.util.List;

public interface UserDao {
    void add(MyUser myUser);

    void updateUser(MyUser myUser);

    List<MyUser> getAllEmployee();

    void deleteUser(Long id);

    MyUser findUser(Long id);

    MyUser findUser(String username);

}
