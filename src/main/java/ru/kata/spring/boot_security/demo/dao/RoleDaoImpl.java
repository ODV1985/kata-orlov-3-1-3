package ru.kata.spring.boot_security.demo.dao;

import org.springframework.stereotype.Repository;
import ru.kata.spring.boot_security.demo.entity.MyUser;
import ru.kata.spring.boot_security.demo.entity.Role;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
public class RoleDaoImpl implements RoleDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Set<Role> findRole(MyUser myUser) {
        return myUser.getRoleSet();
    }

    @Override
    public Set<Role> findRoleAll() {
        return entityManager
                .createQuery("select r from Role r", Role.class).getResultStream().collect(Collectors.toSet());
    }

    @Override
    public Role findRole(Long id) {
        return entityManager.createQuery("select r from Role r where r.id = :r", Role.class)
                .setParameter("r", id).getSingleResult();
    }

    @Override
    public void deleteRole(Long id) {
        Role role = findRole(id);
        entityManager.remove(role);
    }

    @Override
    public Set<Role> buildRoleSet(ArrayList<Long> role) {
        Set<Role> roleSet = new HashSet<>();

        for (Long x : role) {
            roleSet.add(entityManager.createQuery("select r from Role r where r.id = :r", Role.class)
                    .setParameter("r", x).getSingleResult());
        }

        return roleSet;
    }
}
