package ru.kata.spring.boot_security.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.kata.spring.boot_security.demo.entity.MyUser;
import ru.kata.spring.boot_security.demo.entity.Role;
import ru.kata.spring.boot_security.demo.service.RoleService;
import ru.kata.spring.boot_security.demo.service.UserDetailsServiceImpl;
import ru.kata.spring.boot_security.demo.service.UserService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/admin")
public class RestAdminController {

    private final UserService userService;
    private final RoleService roleService;

    @Autowired
    public RestAdminController(UserService userService, RoleService roleService) {
        this.userService = userService;
        this.roleService = roleService;
    }

    // Метод для главной страницы, сразу при помощи View отображаю весь список из БД
    @GetMapping("/index")
    public String indexPage(Model model) {
        List<Role> roleList = roleService.findRoleAll().stream().toList();
        model.addAttribute("empAll", userService.getAllEmployee());
        model.addAttribute("userLog", UserDetailsServiceImpl.getUserLog().getEmail());
        model.addAttribute("roleList", roleList);
        model.addAttribute("user", new MyUser());
        model.addAttribute("rol", roleList.stream()
                .map(Role::getNameRole).collect(Collectors.joining(", ")));
        return "pageAdmin";
    }

    //Добавляю в форму Вьюшки с дефолтными значениями (новые значения), создаю новый объект!
    //Возвращяюсь на главную страницу
    @PostMapping("/addUser")
    public String addUser(@Valid @ModelAttribute("user") MyUser myUser, BindingResult bindingResult,
                          @RequestParam(value = "rol", required = false) ArrayList<Long> role,
                          Model model) {
        MyUser useradd = userService.userForSave(myUser, roleService.buildRoleSet(role));
        model.addAttribute("roleList", roleService.findRoleAll().stream().toList());

        System.out.println(bindingResult.hasErrors());

        if (bindingResult.hasErrors()) {
            return "checkUser";
        }
        if (useradd != null) {
            userService.add(useradd);
            return "redirect:/admin/index";
        } else return "error";
    }

    // Изменяю
    @PatchMapping("/update")
    public String updateEmployee(@RequestParam(value = "rolEdit", required = false) ArrayList<Long> role,
                                 @Valid @ModelAttribute("user") MyUser myUser, BindingResult bindingResult) {
        MyUser userUpdate = userService.userForUpdate(myUser, roleService.buildRoleSet(role));

        if (bindingResult.hasErrors()) {
            return "checkUser";
        }
        if (userUpdate != null) {
            userService.updateUser(userUpdate);
            return "redirect:/admin/index";
        } else return "error";
    }

    // Удаляю Юзера
    @DeleteMapping("/delete/user")
    public String deleteEmployee(@RequestParam("deleteIdUser") Long id) {
        userService.deleteUser(id);
        return "redirect:/admin/index";
    }

}
