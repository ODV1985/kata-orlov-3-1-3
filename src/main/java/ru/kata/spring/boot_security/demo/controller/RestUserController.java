package ru.kata.spring.boot_security.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.kata.spring.boot_security.demo.entity.Role;
import ru.kata.spring.boot_security.demo.service.RoleService;
import ru.kata.spring.boot_security.demo.service.UserDetailsServiceImpl;
import ru.kata.spring.boot_security.demo.service.UserService;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/users")
public class RestUserController {

    private final UserService userService;
    private final RoleService roleService;

    @Autowired
    public RestUserController(UserService userService, RoleService roleService) {
        this.userService = userService;
        this.roleService = roleService;
    }

    @GetMapping("/userIndex")
    public String shouPage(Model model) {
        List<Role> roleList = roleService.findRoleAll().stream().toList();
        model.addAttribute("empAllUser", userService.getAllEmployee());
        model.addAttribute("userLog", UserDetailsServiceImpl.getUserLog().getEmail());
        model.addAttribute("rol", roleList.stream()
                .map(Role::getNameRole).collect(Collectors.joining(", ")));
        return "userPage";
    }

}
